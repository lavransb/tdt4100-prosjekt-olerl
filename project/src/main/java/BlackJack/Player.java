package BlackJack;

public class Player {

	private int chipCount;
	private int currentBet = 0;
	private Hand hand;
	private BlackJack blackJack;
	
	public Player() {
		this.chipCount = 1000;
		this.hand = new Hand();
	}
	
	public void setGame(BlackJack blackJack) {
		this.blackJack = blackJack;
	}
	
	public Hand getCurrentHand() {
		return hand;
	}
	
	public int getChipCount() {
		return chipCount;
	}
	
	public int getCurrentBet() {
		return currentBet;
	}
	
	public void placeBet(String bet) {
		try {
			int intBet = Integer.parseInt(bet);
			if (intBet <= 0) {
				blackJack.setPlayerMessage("Bet must be positive.");
				throw new IllegalArgumentException();
			}
			if(intBet > getChipCount()) {
				blackJack.setPlayerMessage("Bet can not be larger than your chip count.");
				throw new IllegalStateException();
			}
			chipCount -= intBet;
			currentBet = intBet;
		}
		catch(NumberFormatException nfe) {
			blackJack.setPlayerMessage("Bet must be an integer.");
			throw new IllegalArgumentException();
		}
	}
	
	public void addCardToHand(Card card) {
		hand.addCardToHand(card);
	}
	
	public void emptyHand() {
		hand.emptyHand();
	}
	
	public void lostHand() {
		blackJack.addToPlayerHistory("-" + currentBet);
		currentBet = 0;
		if(hand.notBust()) {
			blackJack.setPlayerMessage("You lost the hand... :(");
		} else {
			blackJack.setPlayerMessage("BUSTED! You lost the hand...");
		}
	}
	
	public void wonHand() {
		blackJack.addToPlayerHistory("+" + currentBet);
		chipCount += 2*currentBet;
		currentBet = 0;
		blackJack.setPlayerMessage("Congratulations! You won the hand!");
	}
	
	public void tieHand() {
		blackJack.addToPlayerHistory("+0");
		chipCount += currentBet;
		currentBet = 0;
		blackJack.setPlayerMessage("The hand was tied.");
	}
	
	public void setChipCount(int chipCount) {
		if(chipCount < 0) {
			throw new IllegalArgumentException("Chip count must be positive.");
		}
		this.chipCount = chipCount;
	}
	
}
