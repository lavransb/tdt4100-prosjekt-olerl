package BlackJack;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application{

	@Override
	public void start(final Stage primaryStage)	throws Exception {
		primaryStage.setTitle("BlackJack");
		Parent parent = FXMLLoader.load(App.class.getResource("Game.fxml"));
		primaryStage.setResizable(false);
		primaryStage.setScene(new Scene(parent));
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(App.class, args);
	}

}
