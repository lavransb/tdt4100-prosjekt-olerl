package BlackJack;

import java.io.File;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class GameController {
	
	private BlackJack blackJack;
	private FileChooser fileChooser = new FileChooser();
	private final BlackJackFileReadingInterface fileSupport = new BlackJackFileSupport();
	
	@FXML AnchorPane anchorPane;
	@FXML Button hitButton, standButton, betButton, browseButton, loadButton, saveButton, newGameButton;
	@FXML TextField betAmount, fileLocation;
	@FXML TextArea chipCount, currentBet, message, playerCard1, playerCard2, playerCard3, playerCard4, playerCard5, dealerCard1, dealerCard2, dealerCard3, dealerCard4, dealerCard5;
	@FXML ListView<String> playerHistoryList;
	
	
	@FXML
	public void initialize() {
		blackJack = new BlackJack();
		betButton.setDisable(true);
		hitButton.setDisable(true);
		standButton.setDisable(true);
		saveButton.setDisable(true);
		message.setVisible(false);
		message.setEditable(false);
		updateView();
		fileChooser.setInitialDirectory(new File("C:\\"));
	}
	
	public void setBlackJack(BlackJack blackJack) {
		this.blackJack = blackJack;
		updateView();
		updatePlayerHistoryView();
	}
	
	@FXML
	public void handleHitButton() {
		blackJack.playerHit();
		if(blackJack.getPlayerHandSize() == 3) {
			playerCard3.setText(blackJack.getPlayerHand().getCard(2).toString());
			playerCard3.setVisible(true);
		}
		if(blackJack.getPlayerHandSize() == 4) {
			playerCard4.setText(blackJack.getPlayerHand().getCard(3).toString());
			playerCard4.setVisible(true);
		}
		if(blackJack.getPlayerHandSize() == 5) {
			playerCard5.setText(blackJack.getPlayerHand().getCard(4).toString());
			playerCard5.setVisible(true);
			dealerPlayAndUpdateView();
			return;
		}
		if(!blackJack.playerNotBust()) {
			dealerPlayAndUpdateView();
			
		}
	}
	
	@FXML
	public void handleStandButton() {
		dealerPlayAndUpdateView();
	}
	
	@FXML
	public void handleBetButton() {
		message.clear();
		clearCards();
		
		try {
			message.setVisible(false);
			blackJack.playerBet(betAmount.getText());
			blackJack.dealHands();
			playerCard1.setText(blackJack.getPlayerHand().getCard(0).toString());
			playerCard2.setText(blackJack.getPlayerHand().getCard(1).toString());
			dealerCard1.setText(blackJack.getDealerHand().getCard(0).toString());
			dealerCard2.setText("           ?");
			betAmount.setEditable(false);
			updateView();
			hitButton.setDisable(false);
			standButton.setDisable(false);
			saveButton.setDisable(true);
			loadButton.setDisable(true);
		}
		catch(IllegalArgumentException | IllegalStateException ex) {
			updateMessageView();
		}
		finally {
			betAmount.clear();	
		}
	}
	
	
	public void dealerPlayAndUpdateView() {
		blackJack.dealerPlay();
		hitButton.setDisable(true);
		standButton.setDisable(true);
		dealerCard2.setText(blackJack.getDealerHand().getCard(1).toString());
		if(blackJack.getDealerHandSize() == 3) {
			dealerCard3.setText(blackJack.getDealerHand().getCard(2).toString());
			dealerCard3.setVisible(true);
		}
		if(blackJack.getDealerHandSize() == 4) {
			dealerCard3.setText(blackJack.getDealerHand().getCard(2).toString());
			dealerCard4.setText(blackJack.getDealerHand().getCard(3).toString());
			dealerCard3.setVisible(true);
			dealerCard4.setVisible(true);
		}
		if(blackJack.getDealerHandSize() == 5) {
			dealerCard3.setText(blackJack.getDealerHand().getCard(2).toString());
			dealerCard4.setText(blackJack.getDealerHand().getCard(3).toString());
			dealerCard5.setText(blackJack.getDealerHand().getCard(4).toString());
			dealerCard3.setVisible(true);
			dealerCard4.setVisible(true);
			dealerCard5.setVisible(true);
		}
		blackJack.handFinished();
		updateView();
		updatePlayerHistoryView();
		if(!blackJack.gameOver()) {
			betAmount.setEditable(true);
		}
		updateMessageView();
		saveButton.setDisable(false);
		loadButton.setDisable(false);
	}
	
	@FXML
	public void handleBetAmountTextChange() {
		betButton.setDisable(betAmount.getText() == null || betAmount.getText().isBlank());
	}
	
	public void updateView() {
		chipCount.setText(blackJack.getPlayerChipCount());
		currentBet.setText(blackJack.getPlayerCurrentBet());
	}
	
	public void updateMessageView() {
		message.setVisible(true);
		message.setText(blackJack.getPlayerMessage());
	}
	
	public void updatePlayerHistoryView() {
		playerHistoryList.getItems().setAll(blackJack.getPlayerHistory());
	}
	
	
	public void handleLoadButton() {
		Stage stage = (Stage) anchorPane.getScene().getWindow();
		File file = fileChooser.showOpenDialog(stage);
		String name = file.getName();
		final int pos = name.lastIndexOf('.');
		if(pos >= 0) {
			name = name.substring(0, pos);
		}
		if(! name.isBlank()) {
			name = file.getParent() + File.separator + name;
		}
		setBlackJack(fileSupport.readBlackJack(name, this.blackJack));
		updateMessageView();
		clearCards();
	}
	
	public void handleSaveButton() {
		Stage stage = (Stage) anchorPane.getScene().getWindow();
		fileChooser.setInitialFileName("My Game");
		File file = fileChooser.showSaveDialog(stage);
		String name = file.getName();
		final int pos = name.lastIndexOf('.');
		if(pos >= 0) {
			name = name.substring(0, pos);
		}
		if(! name.isBlank()) {
			name = file.getParent() + File.separator + name;
		}
		blackJack.setName(name);
		fileSupport.writeBlackJack(blackJack);
		updateMessageView();
	}
	
	
	
	public void handleNewGameButton() {
		clearCards();
		initialize();
		updatePlayerHistoryView();
		
	}
	
	public void clearCards() {
		dealerCard1.clear();
		dealerCard2.clear();
		dealerCard3.clear();
		dealerCard4.clear();
		dealerCard5.clear();
		playerCard1.clear();
		playerCard2.clear();
		playerCard3.clear();
		playerCard4.clear();
		playerCard5.clear();
		dealerCard3.setVisible(false);
		dealerCard4.setVisible(false);
		dealerCard5.setVisible(false);
		playerCard3.setVisible(false);
		playerCard4.setVisible(false);
		playerCard5.setVisible(false);
	}
	
	
}
