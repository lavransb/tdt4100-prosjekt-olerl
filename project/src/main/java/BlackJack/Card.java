package BlackJack;

import java.util.Arrays;
import java.util.List;

public class Card {
	
	private char suit;
	private int face;
	
	private List<Character> validSuits = Arrays.asList('S', 'H', 'D', 'C');
	private List<Integer> validFaces = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
	public Card(char suit, int face) {
		if(!(validSuits.contains(suit) && validFaces.contains(face))) {
			throw new IllegalArgumentException("Ilegal values for creating card.");
		}
		this.suit = suit;
		this.face = face;
	}
	
	public char getSuit() {
		return suit;
	}
	
	public int getFace() {
		return face;
	}
	
	// VIKTIG! Sett språk til UTF-8 for at kort symboler skal fungere riktig.
	// club: \u2663, spades: \u2660, diamond: \u2666, heart: \u2665
	// mellomrom i returnert verdi er for at det skal bli estetisk i grensesnittet
	public String toString() {
		String tempSuit = "";
		String tempFace = "";
		switch(suit) {
			case 'S':
				tempSuit += "\u2660";
				break;
			case 'H':
				tempSuit += "\u2665";
				break;
			case 'D':
				tempSuit += "\u2666";
				break;
			case 'C':
				tempSuit += "\u2663";
				break;
		}
		if(face > 10 || face == 1) {
			switch(face) {
				case 11:
					tempFace += "J";
					break;
				case 12:
					tempFace += "Q";
					break;
				case 13:
					tempFace += "K";
					break;
				case 1:
					tempFace += "A";
					break; 
			}
		} else {
			tempFace += face;
		}
		
		return "          " + tempSuit + tempFace;
	}
}
