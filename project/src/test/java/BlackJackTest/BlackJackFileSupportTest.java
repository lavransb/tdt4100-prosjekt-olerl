package BlackJackTest;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import BlackJack.BlackJack;
import BlackJack.BlackJackFileSupport;

public class BlackJackFileSupportTest {

	private BlackJack blackJack;
	private BlackJackFileSupport fileSupport;
	

	@BeforeEach
	public void setup() {
		blackJack = new BlackJack();
		fileSupport = new BlackJackFileSupport();
		blackJack.setName("BlackJackTest");
		blackJack.addToPlayerHistory("+100");
		blackJack.addToPlayerHistory("+10");
		blackJack.addToPlayerHistory("-100");
		blackJack.addToPlayerHistory("-300");
	}
	
	public String getStringRep() {
		return "# name"
				+ System.getProperty("line.separator") + "BlackJackTest"
				+ System.getProperty("line.separator") + "# chip count"
				+ System.getProperty("line.separator") + "1000"
				+ System.getProperty("line.separator") + "# player history"
				+ System.getProperty("line.separator") + "[-300, -100, +10, +100]"
				+ System.getProperty("line.separator");
	}
	
	public String getInvalidStringRep1() {
		return "# name"
				+ System.getProperty("line.separator") + "BlackJackTest"
				+ System.getProperty("line.separator") + "# chip count"
				+ System.getProperty("line.separator") + "absdsd"    //lager en streng med ugyldig chip count 
				+ System.getProperty("line.separator") + "# player history"
				+ System.getProperty("line.separator") + "[-300, -100, +10, +100]"
				+ System.getProperty("line.separator");
	}
	
	public String getInvalidStringRep2() {
		return "# name"
				+ System.getProperty("line.separator") + "BlackJackTest"
				+ System.getProperty("line.separator") + "# chip count"
				+ System.getProperty("line.separator") + "1000"
				+ System.getProperty("line.separator") + "# player history"
				+ System.getProperty("line.separator") + "[-s30df0, -100, +1dsds0, +1f00]"    //lager en streng med ugyldig player history
				+ System.getProperty("line.separator");
	}
	
	@Test
	public void writeBlackJackTest() {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		fileSupport.writeBlackJack(blackJack, os);
		assertEquals(getStringRep(), os.toString());    //sjekker at fileSupport skriver det som er forventet til fil
	}
	
	@Test
	public void readBlackJackTest() throws UnsupportedEncodingException {
		InputStream is = new ByteArrayInputStream(getStringRep().getBytes());
		
		BlackJack actual = fileSupport.readBlackJack(is);
		BlackJack expected = blackJack;
		
		assertEquals(actual.getName(), expected.getName());
		assertEquals(actual.getPlayerChipCount(), expected.getPlayerChipCount());
		assertEquals(actual.getPlayerHistory(), expected.getPlayerHistory());    //sjekker at de relevante verdiene er like i filen som blir lest fra og i den opprinnelige BlackJack filen
	
		InputStream iis1 = new ByteArrayInputStream(getInvalidStringRep1().getBytes());
		BlackJack invalidActual1 = fileSupport.readBlackJack(iis1);
		assertEquals("Corrupted file - game not loaded successfully.", invalidActual1.getPlayerMessage());
		assertEquals("1000", invalidActual1.getPlayerChipCount());
		assertEquals(new ArrayList<String>(), invalidActual1.getPlayerHistory());    //sjekker at read metoden håndterer ugyldige verdier i fil
		
		InputStream iis2 = new ByteArrayInputStream(getInvalidStringRep2().getBytes());
		BlackJack invalidActual2 = fileSupport.readBlackJack(iis2);
		assertEquals("Corrupted file - game not loaded successfully.", invalidActual2.getPlayerMessage());
		assertEquals("1000", invalidActual2.getPlayerChipCount());
		assertEquals(new ArrayList<String>(), invalidActual2.getPlayerHistory());
		
	}
		
}
