package BlackJackTest;


import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import BlackJack.Card;

public class CardTest {

	private Card card;
	
	@Test
	public void constructorTest() {
		Assertions.assertDoesNotThrow(() -> {
			card = new Card('H', 7);
		});
		assertEquals('H', card.getSuit());
		assertEquals(7, card.getFace());    //sjekker at et gyldig kort kan bli lagd og at verdiene blir satt riktig
		
		Assertions.assertThrows(IllegalArgumentException.class, () -> {    //sjekker at ugyldige argumenter utl�ser unntak
			new Card('K', 11);
		});
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new Card('H', 15);
		});
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new Card('S', -1);
		});
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new Card('Q', 14);
		});
	}
	
	@Test
	public void toStringTest() {
		//tester at forskjellige kort f�r riktig streng representasjon
		card = new Card('C', 13);
		assertEquals("          " + "\u2663" + "K", card.toString());
		
		card = new Card('H', 5);
		assertEquals("          " + "\u2665" + "5", card.toString());
		
		card = new Card('D', 11);
		assertEquals("          " + "\u2666" + "J", card.toString());
		
		card = new Card('S', 12);
		assertEquals("          " + "\u2660" + "Q", card.toString());

		card = new Card('C', 1);
		assertEquals("          " + "\u2663" + "A", card.toString());
	}

}
